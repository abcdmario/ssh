FROM centos:7

RUN yum -y update && \
    yum -y install openssh-server && \
    useradd user01 && \
    echo "123" | passwd user01 --stdin && \
    mkdir /home/user01/.ssh && \
    chmod 700 /home/user01/.ssh

ADD user.pub  /home/user01/.ssh/authorized_keys
RUN chown user01:user01 /home/user01/.ssh/authorized_keys && \
    chmod 600 /home/user01/.ssh/authorized_keys 
    
RUN /usr/sbin/sshd-keygen

CMD [ "/usr/sbin/sshd", "-D" ]
